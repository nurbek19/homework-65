import React, { Component } from 'react';
import {Switch, Route} from 'react-router-dom';

import Layout from './components/Layout/Layout';
import './App.css';
import PageContent from "./containers/PageContent/PageContent";
import EditPage from "./containers/EditPage/EditPage";

class App extends Component {
  render() {
    return (
        <Layout>
            <Switch>
                <Route path="/" exact component={PageContent}/>
                <Route path="/pages/admin" component={EditPage}/>
                <Route path="/pages/:page" component={PageContent}/>
            </Switch>
        </Layout>
    );
  }
}

export default App;
