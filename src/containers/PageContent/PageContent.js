import React, {Component} from 'react';
import axios from 'axios';

class PageContent extends Component {
    state = {
        page: {},
        loading: false
    };

    getContentPage = page => {
        axios.get(`/pages/${page}.json`).then(response => {
            this.setState({page: response.data, loading: false});
        })
    };

    componentDidMount() {
        !this.props.match.params.page ?
            this.setState({loading: true}, () => {
                this.getContentPage('home');
            })
            : this.setState({loading: true}, () => {
            this.getContentPage(this.props.match.params.page);
        })
    }

    componentDidUpdate(prevProps) {
        let currentPage = this.props.match.params.page;

        if (prevProps.match.params.page !== currentPage) {
            this.setState({loading: true}, () => {
                !currentPage ? currentPage = 'home' : null;
                this.getContentPage(currentPage);
            })
        }
    }

    render() {
        if (!this.state.loading) {
            return (
                <div className="page-content">
                    <h2>{this.state.page.title}</h2>
                    <p>{this.state.page.content}</p>
                </div>
            )
        } else {
            return <p>Loading...</p>
        }
    }
}

export default PageContent;