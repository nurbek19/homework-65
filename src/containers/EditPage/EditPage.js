import React, {Component} from 'react';
import axios from 'axios';
import './EditPage.css';

class EditPage extends Component {
    state = {
        currentPage: {
            title: '',
            content: ''
        },
        page: 'null',
        loading: false
    };

    changeValue = event => {
        const name = event.target.name;
        this.setState({currentPage: {...this.state.currentPage, [name]: event.target.value}});
    };

    changePage = event => {
        this.setState({page: event.target.value, loading: true});

        axios.get(`/pages/${event.target.value}.json`).then(response => {
            this.setState({currentPage: response.data, loading: false});
        });
    };

    editPage = event => {
        event.preventDefault();
        this.setState({loading: true});
        axios.put(`/pages/${this.state.page}.json`, this.state.currentPage).then(() => {
            this.setState({loading: false});

            if (this.state.page === 'home') {
                this.props.history.replace('/');
            } else {
                this.props.history.replace(`/pages/${this.state.page}`);
            }
        });
    };


    render() {
        if (!this.state.loading) {
            return (
                <form action="#" className="edit-page">
                    <legend>Edit Page</legend>

                    <label>Category</label>

                    <select name="page"
                            onChange={this.changePage}
                            value={this.state.page}
                    >
                        <option value="null">Choose page</option>
                        <option value="home">Home</option>
                        <option value="about">About</option>
                        <option value="contacts">Contacts</option>
                        <option value="divisions">Divisions</option>
                    </select>

                    <label>Title</label>
                    <input type="text" name="title"
                           value={this.state.currentPage.title}
                           onChange={this.changeValue}
                    />


                    <label>Quote text</label>
                    <textarea name="content"
                              cols="30" rows="10"
                              value={this.state.currentPage.content}
                              onChange={this.changeValue}
                    />


                    <button onClick={this.editPage}>Save</button>

                </form>
            )
        } else {
            return <p>Loading...</p>
        }
    }
}

export default EditPage;