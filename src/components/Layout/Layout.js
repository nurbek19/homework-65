import React, {Fragment} from 'react';
import {NavLink} from 'react-router-dom';
import './Layout.css';

const Layout = props => {
    return (
        <Fragment>
            <header className="navigation-bar">
                <h2>Static pages</h2>

                <nav>
                    <ul className="menu">
                        <li>
                            <NavLink to="/">Home</NavLink>
                        </li>
                        <li>
                            <NavLink to="/pages/about">About</NavLink>
                        </li>
                        <li>
                            <NavLink to="/pages/contacts">Contacts</NavLink>
                        </li>
                        <li>
                            <NavLink to="/pages/divisions">Divisions</NavLink>
                        </li>
                        <li>
                            <NavLink to="/pages/admin">Admin</NavLink>
                        </li>
                    </ul>
                </nav>
            </header>
            <main className="Layout-Content">
                {props.children}
            </main>
        </Fragment>
    )
};

export default Layout;